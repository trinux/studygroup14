#!/usr/bin/env python
import sys

# Customers credential and balance
c_dict = {'wubic':['1337',1000]}

# Global temporary variables
choice		= 0
logged_in	= False

# Main loop
while True:
	
	# This will only be displayed if the user is authenticated
	if logged_in:
		while True:
			print '\n1, withdraw'
			print '2, deposit'
			print '3, balance'
			print '4, logout'
			
			if username == 'wubic':
				print '5, Users'
			
			choice = int(raw_input('choice: '))
			
			# withdraw money
			if choice == 1:
				money = int(raw_input('Amount: '))
				
				if money > c_dict[username][1]:
					print '\nFailed: Your withdraw is higher then your balance.\n'
				else:
					c_dict[username][1] -= money
			
			# Deposit money
			elif choice == 2:
				money = int(raw_input('Deposit: '))
				if money > 0:
					c_dict[username][1] += money
			
			# Check balance
			elif choice == 3:
				print '\nBalance: %d \n' % c_dict[username][1]
			
			# Logout current user
			elif choice == 4:
				logged_in = False
				break
			
			# Administrativ tasks
			elif choice == 5 and username in ('wubic'):
				while True:
					print '\n1, Add user'
					print '2, del user'
					print '3, list users'
					print '4, Go back'
					print '5, Exit ATM software'
				
					choice = int(raw_input('choice: '))
				
					# Add users credentials and setup balance
					if choice == 1:
						username = raw_input('Username: ')
						password = raw_input('Password: ')
						
						if username not in c_dict:
							c_dict[username] = [password,0]
					
					# Delete users credentials and balance
					elif choice == 2:
						username = raw_input('Username: ')
						del c_dict[username]
			
					# Check users
					elif choice == 3:
						print 'Users:',
						for i in c_dict:
							print i,
						
						print ''
			
					elif choice == 4:
						break
					
					elif choice == 5:
						sys.exit(0)

	# Login screen and authentication check
	else:
		print '\nWelcome to your local ATM machine\n'
		
		username = raw_input('Username: ')
		password = raw_input('Password: ')
		
		if username in c_dict and c_dict[username][0] == password:
			logged_in = True
		
