#!/usr/bin/env python

print '\nWelcome to your local ATM machine\n'

# Global variables
choice = 0
balance = 0

while True:

	print '1, withdraw'
	print '2, deposit'
	print '3, balance'
	print '4: logout'
	
	# Input from users
	choice = int(raw_input('choice: '))
		
	print '\n'
	
	# Withdraw money
	if choice == 1:
		money = int(raw_input('Amount: '))
		if money > balance:
			print "\nFailed: Your withdraw is to high.\n"
		else:
			balance -= money
		
		choice = 0
		
	# Deposit money
	elif choice == 2:
		money = int(raw_input('Deposit: '))
		if money > 0:
			balance += money

		choice = 0
	
	# Check balance
	elif choice == 3:
		print '\nBalance: %d\n' % (balance) 
		
		choice = 0
	
	# Quit application
	elif choice == 4:
		break